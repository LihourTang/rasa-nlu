FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8
ENV PYTHONUNBUFFERED True
ADD requirements.txt /
RUN pip install -r /requirements.txt
COPY . /var/www
WORKDIR /var/www
ENTRYPOINT [ "rasa", "run", "-p", "8080", "--enable-api", "--cors", "*", "--debug" ]